toolbox_indicator () {
    if [ ! -z "$container" ] && [ ! -z "$DISTTAG" ]; then
        echo -e "\e[31m●\e[0m "
    fi
}

git_branch () {
    local branch="$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')"

    if [ ! -z "$branch" ]; then
        echo -e " \e[35m\e[37;45m $branch\e[0;35m\e[0m"
    fi
}

git_tag () {
    local tag="$(git describe --tags --abbrev=0 2> /dev/null)"

    if [ ! -z "$tag" ]; then
        echo -e " \e[36m\e[37;46m $tag\e[0;36m\e[0m"
    fi
}


PS1=""

# new line
PS1+="\n"

# Toolbox indicator
PS1+='$(toolbox_indicator)'

# current path
PS1+="\e[33m\w\e[0m"

# git
PS1+='$(git_branch)$(git_tag)'

# prompt
PS1+="\n "

export PS1